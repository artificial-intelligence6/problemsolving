﻿using System.Numerics;

namespace ProblemSolving.PuzzleProblem
{
    public static class Directions
    {
        public static readonly Vector2 UP    = new Vector2( 0,-1);
        public static readonly Vector2 DOWN  = new Vector2( 0, 1);
        public static readonly Vector2 LEFT  = new Vector2(-1, 0);
        public static readonly Vector2 RIGHT = new Vector2(1,  0);
    }

    public class Cursor {
        
        Vector2 Position { get; set; }
//        Vector2[] Path { get; set; }


        void Move(Vector2 direction)
        {
            if (direction == Directions.UP)
            { }
            else if (direction == Directions.DOWN)
            { }
            else if (direction == Directions.LEFT)
            { }
            else if (direction == Directions.RIGHT)
            { }
            else
            {
                // do nothing
            }
        }
    }
}
