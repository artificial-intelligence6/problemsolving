﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProblemSolving.PuzzleProblem
{

    public class PuzzleBoard
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public int[,] Board { get; set; }

        public PuzzleBoard(int width, int height)
        {
            Width = width;
            Height = height;

            Board = new int[width, height];
        }

        public void GenerateInitialRandomState()
        {
            List<int> numbersToSet = new List<int>();
            for (int i = 0; i < (Width * Height); ++i)
                numbersToSet.Add(i);
            Random random = new Random();
            for (int i = 0; i < Board.GetLength(0); ++i)
                for (int y = 0; y < Board.GetLength(1); ++y)
                {
                    var index = random.Next(numbersToSet.Count);
                    Board[i, y] = numbersToSet[index];
                    numbersToSet.Remove(numbersToSet[index]);
                }
        }

        public void PrintBoard()
        {
            for (int i = 0; i < Board.GetLength(0); ++i)
            {
                for (int y = 0; y < Board.GetLength(1); ++y)
                {
                    Debug.Write(Board[i, y]);
                    Debug.Write(' ');
                }
                Debug.Write('\n');
            }
        }

        public void Move(string direction)
        {
            int col = 0;
            int line = 0;

            for (int i = 0; i < Board.GetLength(0); ++i)
                for (int y = 0; y < Board.GetLength(1); ++y)
                {
                    if (Board[i, y].Equals(0))
                    {
                        line = i;
                        col = y;
                    }
                }

            switch (direction)
            {
                case "up":
                    if (line - 1 >= 0)
                    {
                        int tmp = Board[line - 1, col];
                        Board[line - 1, col] = 0;
                        Board[line, col] = tmp;
                        Debug.Write("Moved UP");
                    }
                    else
                        Debug.Write("Can't move UP");
                    break;
                case "down":
                    if (line + 1 < Board.GetLength(0))
                    {
                        int tmp = Board[line + 1, col];
                        Board[line + 1, col] = 0;
                        Board[line, col] = tmp;
                        Debug.Write("Moved DOWN");
                    }
                    else
                        Debug.Write("Can't move DOWN");
                    break;
                case "left":
                    if (col - 1 >= 0)
                    {
                        int tmp = Board[line, col - 1];
                        Board[line, col - 1] = 0;
                        Board[line, col] = tmp;
                        Debug.Write("Moved LEFT");
                    }
                    else
                        Debug.Write("Can't move LEFT");
                    break;
                case "right":
                    if (col + 1 <= Board.GetLength(1))
                    {
                        int tmp = Board[line, col + 1];
                        Board[line, col + 1] = 0;
                        Board[line, col] = tmp;
                        Debug.Write("Moved RIGHT");
                    }
                    else
                        Debug.Write("Can't move RIGHT");
                    break;
            }
        }

    }
}
