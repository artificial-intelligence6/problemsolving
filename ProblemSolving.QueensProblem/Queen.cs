﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.QueensProblem
{
    public class Queen : Tuple<int, int>, IQueen
    {
        public Queen(Board board, int row, int col) : base(row, col)
        {
            this.Board = board;
        }
        
        public Board Board { get; }
        public int Row => Item1;
        public int Column => Item2;
        
        public bool IsAttacked() => Board.IsAttacked(Row, Column);
        public bool IsAttackedAbove() => Board.IsAttackedAbove(Row, Column);
        public int AttackScore() => Board.AttackScore(Row, Column);
    }
}
