﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.QueensProblem
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBoard
    {
        int Size { get; }
        int Count { get; }
        IEnumerable<int> Rows { get; }

        IEnumerable<IQueen> GetQueens();

        bool IsAttacked(int row, int col);
        bool IsAttacked(int row);

        int AttackedQueens();
        IEnumerable<IQueen> GetAttackedQueens();

        int AttackScore(int row, int col);
        int AttackScore(int row);
        int AttackScore();
    }
}
