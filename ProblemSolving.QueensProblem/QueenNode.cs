﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ProblemSolving.QueensProblem
{
    public class QueenNode : Queen, INode<QueenNode>
    {
        public QueenNode(Board board, int row, int col) : base(board, row, col)
        { }

        public QueenNode Next()
        {
            var newColumn = Column + 1;
            if (newColumn == Board.Size)
            {
                return null;
            }
            var nextNode = new QueenNode(Board, Row, newColumn);
            Board.Queens[Row] = newColumn;
            return nextNode;
        }

        public IEnumerable<QueenNode> Iterate()
        {
            var node = this;

            while (node != null)
            {
                yield return node;
                node = node.Next();
            }
        }

        public QueenNode Child()
        {
            int newRow = Row + 1;
            if (newRow != Board.Size)
            {
                var nextNode = new QueenNode(Board, newRow, 0);
                Board.Queens[newRow] = 0;
                return nextNode;
            }
            else
            { 
                return null;
            }
        }

        public IEnumerable<QueenNode> Children()
        {
            var child = Child();
            return child.Iterate();
        }

        public QueenNode Parent()
        {
            if (this.Row == 0)
            {
                return null;
            }
            else
            {
                return new QueenNode(Board, Row - 1, Column);
            }
        }
    }
}
