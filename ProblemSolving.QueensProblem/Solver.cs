﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.QueensProblem
{
    public class Solver
    {
        public Solver(Board board)
        {
            Board = board;
        }

        public Board Board { get; }

        public bool DepthFirstUninformed(QueenNode queenNode)
        {
            foreach (var node in queenNode.Iterate())
            {
                if (!node.IsAttackedAbove())
                {
                    QueenNode child = node.Child();
                    if (child == null)
                    { 
                        return true;
                    }
                    else
                    {
                        var ret = DepthFirstUninformed(node.Child());
                        if (ret == true) return true;
                    }
                }
            }
            return false;
        }
    }
}
