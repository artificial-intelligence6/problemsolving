﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.QueensProblem
{
    public interface IQueen
    {
        public Board Board { get; }
        public int Row { get; }
        public int Column { get; }

        bool IsAttacked();

        bool IsAttackedAbove();

        int AttackScore();

    }
}
