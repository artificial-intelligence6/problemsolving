﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ProblemSolving.QueensProblem
{
    public class Board
    {

        #region PROPERTIES

        public static Random Seed { get; } = new Random((int)DateTime.Now.Ticks);

        public int[] Queens { get; set; }

        public int Size => Queens.Length;

        #endregion PROPERTIES

        #region CONSTRUCTOR

        public Board(int size)
        {
            Queens = new int[size];
        }

        public Board(int[] queens)
        {
            Queens = queens;
        }

        #endregion CONSTRUCTOR

        #region FACTORIES

        public static Board RandomBoard(int size)
        {
            var board = new Board(size);
            board.Randomize();
            return board;
        }

        public static Board EmptyBoard(int size)
        {
            var board = new Board(size);
            board.Clear();
            return board;
        }

        #endregion FACTORIES

        #region METHODS

        public void Clear(int fill = -1)
        {
            Array.ForEach(Queens, x => x = fill);
        }

        public void Randomize()
        {
            Array.ForEach(Queens, x =>
                {
                    x = Board.Seed.Next();
                });
        }

        public bool IsAttacked(int row, int col)
        {
            for (int y = 0; y < Queens.Length; y++)
            {
                int x = Queens[y];

                // Same row is impossible, also checks if cell is empty
                if (y != row && x != -1)
                {
                    // Same col
                    if (x == col) return true;
                    // Same major diagonal
                    if ((x - col) == (row - y)) return true;
                    // Same minor diagonal
                    if ((col - x) == (row - y)) return true;
                }
            }
            return false;
        }

        public bool IsAttacked(int row) => IsAttacked(row, Queens[row]);

        public bool IsAttackedAbove(int row, int col)
        {
            for (int y = row - 1; y >= 0; y--)
            {
                int x = Queens[y];

                // Same col
                if (x == col) return true;
                // Same major diagonal
                if ((x - col) == (row - y)) return true;
                // Same minor diagonal
                if ((col - x) == (row - y)) return true;
            }
            return false;
        }

        public bool IsAttackedAbove(int row) => IsAttackedAbove(row, Queens[row]);

        public int AttackedQueens()
        {
            int attacked = 0;
            // Explore each row
            for (int y = 0; y < Queens.Length; y++)
            {
                if (IsAttacked(y)) attacked++;
            }
            return attacked;
        }

        public int AttackScore(int row, int col)
        {
            int score = 0;

            for (int y = 0; y < Queens.Length; y++)
            {
                int x = Queens[y];

                // Same row is impossible, also checks if cell is empty
                if (y != row && x != -1)
                {
                    // Same col
                    if (x == col) score++;
                    // Same major diagonal
                    if ((x - col) == (row - y)) score++;
                    // Same minor diagonal
                    if ((col- x) == (row - y)) score++;
                }
            }
            return score;
        }

        public int AttackScore(int row)
        {
            return AttackScore(row, Queens[row]);
        }

        public int AttackScore()
        {
            int score = 0;
            // Explore each row
            for (int y = 0; y < Queens.Length; y++)
            {
                score += AttackScore(y);
            }
            return score;
        }

        #endregion METHODS

    }
}