﻿using System.Collections.Generic;

namespace ProblemSolving.QueensProblem
{
    public interface INode<T>
    {
        IEnumerable<T> Children();
        T Child();
        T Next();
    }
}