﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.QueensProblem
{
    public class Action
    {
        public string Name { get; }
        public string Description { get; }
        public int Count { get; }

        public Action(string name, string description = "", int count = 1)
        {
            this.Name = name;
            this.Description = description;
            this.Count = count;
        }
    }
}
