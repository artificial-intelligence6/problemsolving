using System;
using System.Diagnostics;
using System.Linq;

using ProblemSolving.QueensProblem;

using Xunit;
using Xunit.Abstractions;

namespace ProblemSolving.Tests
{
    public class FourQueensTests
    {

        private readonly ITestOutputHelper output;

        public FourQueensTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        #region UNINFORMED

        [Fact]
        public void UninformedSolving_OnFour_ShouldCorrespondToPattern()
        {
            var pattern = new int[] { 1, 3, 0, 2 };

            Board board = Board.EmptyBoard(4);

            var solver = new Solver(board);

            solver.DepthFirstUninformed(new QueenNode(board, 0, 0));

            Assert.Equal(pattern.AsEnumerable(), board.Queens.AsEnumerable());

        }

        [Fact]
        public void UninformedSolving_OnTwenty_ShouldCorrespondToPattern()
        {
            var pattern = new int[] {
                0,2,4,1,3,12,14,11,17,19,16,8,15,18,7,9,6,13,5,10
            };

            Board board = Board.EmptyBoard(20);

            var solver = new Solver(board);

            solver.DepthFirstUninformed(new QueenNode(board, 0, 0));

            foreach (var q in board.Queens) output.WriteLine(q.ToString());

            Assert.Equal(pattern.AsEnumerable(), board.Queens.AsEnumerable());

        }


        [Fact]
        public void UninformedSolving_OnHundred_ShouldCorrespondToPattern()
        {
            var pattern = new int[] {
                0,2,4,1,3,12,14,11,17,19,16,8,15,18,7,9,6,13,5,10
            };

            Board board = Board.EmptyBoard(100);

            var solver = new Solver(board);

            solver.DepthFirstUninformed(new QueenNode(board, 0, 0));

            foreach (var q in board.Queens) output.WriteLine(q.ToString());

            Assert.True(true);

        }


        #endregion UNINFORMED

    }
}
