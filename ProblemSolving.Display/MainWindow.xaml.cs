﻿using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ProblemSolving.QueensProblem;

using ProblemSolving.Solving;

namespace ProblemSolving.Display
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void EightPuzzleProblem(SearchMethodEnum searchMethod)
        {
            PuzzleProblem.PuzzleBoard board = new PuzzleProblem.PuzzleBoard(3, 3);
            board.GenerateInitialRandomState();
            board.PrintBoard();
            Debug.Write("EightPuzzleProblem : " + searchMethod + "\n");
        }
        private void TwentyfivePuzzleProblem(SearchMethodEnum searchMethod)
        {
            PuzzleProblem.PuzzleBoard board = new PuzzleProblem.PuzzleBoard(5, 5);
            board.GenerateInitialRandomState();
            board.PrintBoard();
            Debug.Write("TwentyfivePuzzleProblem : " + searchMethod + "\n");
        }
        private void TwentyQueensProblem(SearchMethodEnum searchMethod)
        {
            Debug.Write("TwentyQueensProblem : " + searchMethod + "\n");
            Board board = Board.EmptyBoard(20);
            QueenNode queenNode = new QueenNode(board, 0, 0);
            Solver solve = new Solver(board);
            switch (searchMethod)
            {
                // case Other method:
                case SearchMethodEnum.Uninformed:
                    solve.DepthFirstUninformed(queenNode);
                    break;
            }
            foreach (var q in board.Queens)
            {
                Debug.Write(q + '\n');
            }
        }
        private void OneMillionQueensProblem(SearchMethodEnum searchMethod)
        {
            Debug.Write("OneMillionQueensProblem : " + searchMethod + "\n");
            Board board = Board.EmptyBoard(20);
            QueenNode queenNode = new QueenNode(board, 0, 0);
            Solver solve = new Solver(board);
            switch (searchMethod)
            {
                // case Other method:
                case SearchMethodEnum.Uninformed:
                    solve.DepthFirstUninformed(queenNode);
                    break;
            }
            foreach (var q in board.Queens)
            {
                Debug.Write(q + '\n');
            }
        }

        private void StartButton(object sender, RoutedEventArgs e)
        {
            SearchMethodEnum method = (SearchMethodEnum)searchMethod.SelectedItem;
            var checkedValue = radioPanel.Children.OfType<RadioButton>().FirstOrDefault(r => r.IsChecked.HasValue && r.IsChecked.Value);

            if (checkedValue.Name == "Radio1")
                EightPuzzleProblem(method);
            else if (checkedValue.Name == "Radio2")
                TwentyfivePuzzleProblem(method);
            else if (checkedValue.Name == "Radio3")
                TwentyQueensProblem(method);
            else if (checkedValue.Name == "Radio4")
                OneMillionQueensProblem(method);
        }
    }
}
