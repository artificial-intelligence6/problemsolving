﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Markup;

namespace ProblemSolving.Display
{
    public class EnumBinding : MarkupExtension
    {
        public Type EnumType { get; private set; }
        public EnumBinding(Type enumType)
        {
            if (enumType is null || !enumType.IsEnum)
                throw new Exception("EnumType null or not enum type");
            EnumType = enumType;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Enum.GetValues(EnumType);
        }
    }
}
