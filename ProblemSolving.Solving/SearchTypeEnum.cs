﻿namespace ProblemSolving.Solving
{
    public enum SearchTypeEnum
    {
        DepthFirst,
        BreadthFirst
    }
}
