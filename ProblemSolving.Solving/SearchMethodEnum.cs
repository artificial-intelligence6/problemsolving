﻿namespace ProblemSolving.Solving
{
    public enum SearchMethodEnum
    {
        Uninformed,
        Informed,
        Local,
    }
}
