using System;

namespace ProblemSolving.Solving
{
	public class PathNodeInfo<T>
    {
        GraphNode<T> previous;

        public PathNodeInfo(GraphNode<T> previous)
        {
            this.previous = previous;
        }

        public GraphNode<T> Previous
        {
            get { return previous; }
        }
    }
}